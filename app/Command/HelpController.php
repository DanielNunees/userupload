<?php

namespace App\Command;

use Cli\CommandController;
use Cli\Helpers\Commands\CommandHelper;

class HelpController extends CommandController
{
    public function handle()
    {
        $this->ivalidParams();
        $this->findHelper();
    }

    /**
     * Check if there's any invalid values
     * otherwise call the helpers
     */
    public function ivalidParams()
    {
        $invalid_params = $this->getInvalidCommands();
        if (count($this->getInvalidCommands()) > 0) {
            $this->callHelper('invalid', $invalid_params);
            exit;
        }
    }

    public function findHelper()
    {
        $need_help = $this->getArgvParams();
        if (count($need_help) == 1) {
            $need_help = array('help');
        }

        // if the commmand help is not runnning by it self remove it from the call list
        if (count($need_help) > 1 && ($key = array_search('help', $need_help)) !== false) {
            unset($need_help[$key]);
        }

        foreach ($need_help as $help) {
            $this->callHelper($help);
        }
    }

    public function callHelper($helper, $data = null)
    {
        $helperClass = CommandHelper::getHelper($helper);
        $helperClass->help($data);
    }

}
