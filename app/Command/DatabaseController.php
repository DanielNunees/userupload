<?php

namespace App\Command;

use Cli\CommandController;
use Cli\DatabaseService;
use Cli\Helpers\Validators\DatabaseConfigValidator;

class DatabaseController extends CommandController
{

    protected $username;
    protected $password;
    protected $host;
    public $databaseService;

    public function handle()
    {
        DatabaseConfigValidator::validate($this->getParamsKeys(), $this->getParams());
        $this->setParams();
        $this->createDatabase();
    }

    /**
     * connects with MySql and create a new database the name is define on config.php
     */
    public function createDatabase()
    {
        $instance = DatabaseService::getInstance();
        $instance->connectDatabase($this->username, $this->password, $this->host);
        $instance->createDatabase();
    }

    public function setParams()
    {
        $this->setDatabaseHost();
        $this->setDatabasePassword();
        $this->setDatabaseUsername();
    }

    public function setDatabaseUsername()
    {
        $this->username = $this->hasParam('u') ? $this->getParam('u') : null;
    }

    public function setDatabasePassword()
    {
        $this->password = $this->hasParam('p') ? $this->getParam('p') : null;
    }

    public function setDatabaseHost()
    {
        $this->host = $this->hasParam('h') ? $this->getParam('h') : null;
    }
}
