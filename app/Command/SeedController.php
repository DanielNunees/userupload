<?php

namespace App\Command;

use Cli\CommandController;
use Cli\DatabaseService;
use Cli\Helpers\FileReader\FileReader;
use Cli\ModelService;

class SeedController extends CommandController
{
    public function handle()
    {
        $fileReader = FileReader::getInstance();
        $instance = DatabaseService::getInstance();
        $this->insertData($instance, $fileReader);
    }

    public function insertData($instance, $fileReader)
    {
        $models = $this->createUserModels($fileReader);
        $instance->runSeeder($models);
    }

    public function createUserModels(FileReader $fileReader)
    {
        return ModelService::createUserModelFromCsvFile($fileReader->getSanitazedData(), $fileReader->getColumns(), $fileReader->getFileName());
    }

}
