<?php

namespace App\Command;

use Cli\CommandController;
use Cli\DatabaseService;

class CreateTableController extends CommandController
{
    public function handle()
    {
        $instance = DatabaseService::getInstance();
        $instance->runMigrations();
    }
}
