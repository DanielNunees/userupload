<?php

namespace App\Command;

use Cli\CommandController;
use Cli\Helpers\FileReader\FileReader;
use Cli\Helpers\Validators\FileValidator;

class FileController extends CommandController
{
    public function handle()
    {
        FileValidator::validate($this->getParamsKeys(), $this->getParams()); //validate the --file command params
        $this->readFile($this->getParam('file'));
    }

    /**
     * Run the read file function
     * sanitizes the data that is
     * read for database
     *
     */
    public function readFile()
    {
        $fileReader = FileReader::getInstance();
        $fileReader->read($this->getParam('file'));
    }
}
