<?php

namespace Cli;

use Cli\Helpers\Validators\CommandValidator;

/**
 * Simple abstraction to the command call iy provides a way to parse named parameters
 */
class CommandCall
{
    public $options = [];

    private $configOptions = [];

    public $params = [];

    public $command = [];

    private $invalidCommands = [];

    private $argvOptions;

    public $subcommand;

    public function __construct($argv)
    {
        $config = include 'config.php';
        $this->setConfigOptions(array_merge($config['longopts'], $config['shortoptsarray']));
        $this->mapCommandController($config, $argv);
    }

    public function mapCommandController(array $config, $argv)
    {
        // get all the options from the command line
        // doing this, does not matter if the user pass extra values,
        // we will get only the values in the config file
        $options = getopt($config['shortopts'], $config['longopts']);

        // check if theres any extra or wrong commnads

        $validatorResult = CommandValidator::validate($argv, $this->getConfigOptions(), $options);
        if (count($validatorResult['invalid']) > 0 || count($validatorResult['missingCommnands']) > 0) {
            $this->setInvalidCommands($validatorResult['invalid']);
            $this->setArgvParams($validatorResult['missingCommnands']);
            $this->addCommand('help');
            return;
        }

        foreach ($options as $key => $data) {
            $this->setOptionData($key, $data);
        }

        $optionKeys = array_keys($options); // get only the keys
        if ($this->isHelp($optionKeys)) {
            $this->setArgvParams(array_keys($options));
            $this->addCommand('help');
            return;
        }

        if ($this->isDryRun($optionKeys)) {
            $this->addCommand('database'); // Create the database
            $this->addCommand('file'); // Reads the file
            $this->addCommand('createtable'); //Create the table
            return;
        }

        if ($this->isCreateTable($optionKeys)) {
            $this->addCommand('database'); // Create the database
            $this->addCommand('file'); // Reads the file
            $this->addCommand('createtable'); //Create the table
            $this->addCommand('seed'); // InsertTable the data on the database
            return;
        }

    }

    /**
     * Check if is a dry run
     */
    public function isDryRun($options)
    {
        return in_array('dry_run', $options);
    }

    /**
     * Check if is a dry run
     */
    public function isCreateTable($options)
    {
        return in_array('create_table', $options);
    }

    /**
     * Check if the user need some help
     */
    public function isHelp($options)
    {
        return in_array('help', $options);
    }

    /**
     * Add new commnad to be executed
     */
    public function addCommand(string $command)
    {
        $this->command[] = $command;
    }

    /**
     * Get the option data
     */
    public function getOptionData(string $option)
    {
        return $this->params[$option];
    }

    /**
     * Set the data of the if any
     */
    public function setOptionData(string $key, string $data)
    {
        $this->params[$key] = $data;
    }

    public function setArgvParams($argments)
    {
        $this->argvOptions = $argments;
    }

    public function getArgvParams()
    {
        return $this->argvOptions;
    }

    /**
     * Chech if the command has a givem param
     */
    public function hasParam($param)
    {
        return isset($this->params[$param]);
    }

    /**
     * set the configurations stored on config.php
     */
    public function setConfigOptions($configOptions)
    {
        $this->configOptions = $configOptions;
    }

    /**
     * set the configurations stored on config.php
     */
    public function getConfigOptions()
    {
        return $this->configOptions;
    }

    /**
     * Get a param givem the option
     */
    public function getParam($param)
    {
        return $this->hasParam($param) ? $this->params[$param] : null;
    }

    public function getInvalidCommands()
    {
        return $this->invalidCommands;
    }

    public function setInvalidCommands($invalidCommands)
    {
        $this->invalidCommands = $invalidCommands;
    }
}
