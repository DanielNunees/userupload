<?php

namespace Cli\Helpers\Formaters;

class CsvFileFormater
{

    public static function formatColumnNames(array $data)
    {
        $formated_columns = [];
        foreach ($data as $column) {
            $formated_columns[] = UserFormater::trimString($column);
        }
        return $formated_columns;
    }
}
