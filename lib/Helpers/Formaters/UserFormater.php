<?php

namespace Cli\Helpers\Formaters;

use Cli\Helpers\CliPrint\CliPrinter;
use Cli\Helpers\Validators\UserDataValidator;

class UserFormater
{

    public static function format(array $data, array $columns)
    {
        $formated_data = [];
        for ($i = 0; $i < count($columns); $i++) {
            $formater = 'format' . ucfirst($columns[$i]);
            $formated_data[] = static::$formater($data[$i]);
        }
        return $formated_data;
    }

    public static function formatEmail($email)
    {
        if (!static::isValidEmail($email)) {
            CliPrinter::display("Warning: invalid email: " . $email);
        } else {
            return static::trimString(strtolower($email));
        }
    }

    /**
     * Check if it is a valida email
     */
    public static function isValidEmail($email)
    {
        return UserDataValidator::validateEmail($email);
    }

    /**
     * Capitalize the name
     */
    public static function formatName($name)
    {
        return static::trimString(ucfirst(strtolower($name)));
    }

    /**
     * Capitalize the surname
     */
    public static function formatSurname($surname)
    {
        return static::trimString(ucfirst(strtolower($surname)));
    }

    /**
     * remove white spaces
     */
    public static function trimString($string)
    {
        return preg_replace('/\s+/', '', $string);
    }
}
