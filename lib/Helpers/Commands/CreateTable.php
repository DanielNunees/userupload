<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class CreateTable extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php --create_table [-u -p -h --file]");
    }
}
