<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class Password extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php -p mysql_password");
    }
}
