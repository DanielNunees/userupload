<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class File extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php --file file.csv");
    }
}
