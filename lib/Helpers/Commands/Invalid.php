<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class Invalid extends CommandHelper
{
    public function help($invalidCommands = null)
    {
        CliPrinter::display("Undentified options in your command: " . implode(", ", $invalidCommands));
    }
}
