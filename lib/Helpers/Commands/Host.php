<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class Host extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php -h host");
    }
}
