<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class Help extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php -u mysql_uername -p mysql_password -h host [--create_table --dry_run --file:] [--help]");
    }
}
