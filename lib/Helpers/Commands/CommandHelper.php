<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;
use Exception;

abstract class CommandHelper
{
    abstract protected function help();

    public static function getHelper($helper)
    {
        $helperClasses = scandir("lib/Helpers/Commands");
        $helper = str_replace("_", "", $helper);
        foreach ($helperClasses as $helperClass) {
            if (strlen($helper) == 1) {
                if ($helper == 'h') {
                    $helperClass = 'host';
                }

                if ($helper == 'u') {
                    $helperClass = 'username';
                }

                if ($helper == 'p') {
                    $helperClass = 'password';
                }
                return static::getHelperClass($helperClass);
            } else {
                return static::getHelperClass($helper);
            }
        }
    }

    public static function getHelperClass($helperClass)
    {
        try {
            $controller_class = str_replace('.php', '', $helperClass);
            $full_class_name = sprintf("Cli\\Helpers\\Commands\\%s", ucfirst($controller_class));
            $class = new $full_class_name();
            return $class;
        } catch (Exception $ex) {
            CliPrinter::display('Error: Helper class ' . $helperClass . ' Not found!');
            exit;
        }

    }
}
