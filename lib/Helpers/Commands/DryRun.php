<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class DryRun extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php --dry_run [-u -p -h --file]");
    }
}
