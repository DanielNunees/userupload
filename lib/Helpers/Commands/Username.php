<?php

namespace Cli\Helpers\Commands;

use Cli\Helpers\CliPrint\CliPrinter;

class Username extends CommandHelper
{
    public function help()
    {
        CliPrinter::display("usage: php user_upload.php -u mysql_username");
    }
}
