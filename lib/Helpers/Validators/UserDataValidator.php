<?php

namespace Cli\Helpers\Validators;

class UserDataValidator
{

    public static function validateEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
}
