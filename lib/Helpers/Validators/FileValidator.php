<?php

namespace Cli\Helpers\Validators;

use Exception;

class FileValidator
{

    /**
     * Check if the command options has any value
     */
    public static function validate(array $optionKeys, array $options)
    {
        if (!in_array('file', $optionKeys)) {
            throw new Exception("Missing --file option, check the command signature" );
        }

        //Check if the extension is .csv
        $file_parts = pathinfo($options['file']);
        if (!array_key_exists('extension', $file_parts) || $file_parts['extension'] !== 'csv') {
            throw new Exception("CSV files only");
        }
        return true;
    }

}
