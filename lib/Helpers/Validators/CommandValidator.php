<?php

namespace Cli\Helpers\Validators;

class CommandValidator
{

    /**
     * Check if the options are inside the range of possible options
     */

    public static function validate($argv, $configOptions, $options)
    {
        // based on original work from the PHP Laravel framework
        if (!function_exists('str_contains')) {
            function str_contains(string $haystack, string $needle)
            {
                return empty($needle) || strpos($haystack, $needle) !== false;
            }
        }
        $validatorResult = array('missingCommnands' => [], 'invalid' => []);
        $okOptions = array_merge(array_keys($options), array_values($options));
        $invalidOptions = (array_diff(static::helper($argv), $okOptions));

        if (count($invalidOptions) > 0) {
            $validatorResult['invalid'] = $invalidOptions;
        }
        if (count($options) !== count($configOptions)) {
            $validatorResult['missingCommnands'] = static::validateOptionPairs($options, $configOptions);
        }

        return $validatorResult;
    }

    public static function validateOptionPairs($options, $configOptions)
    {
        $filteredDryRun = static::isDryRun(array_keys($options));
        $filteredCreateTable = static::isCreateTable($filteredDryRun);
        return array_diff(static::sanitazeConfigOptions($filteredCreateTable), array_keys($options));
    }

    public static function isDryRun($configOptions)
    {
        if ((array_search('dry_run', $configOptions)) !== false) {
            if (($key = array_search('create_table', $configOptions)) !== false) {
                unset($configOptions[$key]);
            }
            if (($key = array_search('help', $configOptions)) !== false) {
                unset($configOptions[$key]);
            }
        }

        return $configOptions;
    }

    public static function isCreateTable($configOptions)
    {
        if ((array_search('create_table', $configOptions)) !== false) {
            if (($key1 = array_search('dry_run', $configOptions)) !== false) {
                unset($configOptions[$key1]);
            }
            if (($key2 = array_search('help', $configOptions)) !== false) {
                unset($configOptions[$key2]);
            }
        }
        return $configOptions;
    }

    public static function sanitazeOptions($argv)
    {
        $sanitazedOpts = [];
        array_shift($argv);
        foreach ($argv as $value) {
            if (str_contains($value, "--") or str_contains($value, "-")) {
                $command = str_replace(["-", "--"], "", $value);
                $sanitazedOpts[] = $command;
            }
        }
        return $sanitazedOpts;
    }

    public static function sanitazeConfigOptions($configOptions)
    {
        $sanitazedOpts = [];
        foreach ($configOptions as $value) {
            $command = str_replace(["-", "--", ':'], "", $value);
            $sanitazedOpts[] = $command;
        }
        return $sanitazedOpts;
    }

    public static function helper($argv)
    {
        $sanitazedOpts = [];
        array_shift($argv);
        foreach ($argv as $value) {
            if (str_contains($value, "--") or str_contains($value, "-")) {
                $command = str_replace(["-", "--"], "", $value);
                $sanitazedOpts[] = $command;
            } else {
                $sanitazedOpts[] = $value;
            }
        }
        return $sanitazedOpts;

    }
}
