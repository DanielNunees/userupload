<?php

namespace Cli\Helpers\Validators;

use Exception;

class DatabaseConfigValidator
{

    /**
     * Check if the command options has any value
     * For the database check, all options must be filled with
     * some data
     */
    public static function validate(array $optionsKeys, array $options)
    {
        $config = include 'config.php';
        if (count(array_intersect($optionsKeys, $config['shortoptsarray'])) != count($config['shortoptsarray'])) {
            throw new Exception("Missing databse config parameters, check the command signature");
        }
        foreach (array_intersect($optionsKeys, $config['shortoptsarray']) as $value) {
            if (empty($options[$value])) {
                throw new Exception("Missing databse config parameters, check the command signature");
            }
        }
        return true;
    }

}
