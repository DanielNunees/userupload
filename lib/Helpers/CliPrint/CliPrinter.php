<?php

namespace Cli\Helpers\CliPrint;

class CliPrinter
{
   /**
    * print message on the command line
    */
    public static function out($message)
    {
        echo $message;
    }

    /**
     * Create a new line for better visualization
     */
    public static function newline()
    {
        static::out("\n");
    }

    /**
     * Display a new test with two line separating the message
     */
    public static function display($message)
    {
        static::newline();
        static::out($message);
        static::newline();
        static::newline();

    }
}
