<?php

namespace Cli\Helpers\FileReader;

use Cli\Helpers\Formaters\CsvFileFormater;
use Cli\Helpers\Formaters\UserFormater;

class FileReader
{

    private $sanitazedData = [];
    private $columns;
    private static $instance = null;
    private $file_name;

    private function __construct()
    {
    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new FileReader();
        }
        return self::$instance;
    }

    /**
     * Read data from a csv file
     */
    public function read(string $file_name)
    {
        if (($handle = fopen($file_name, "r")) !== false) {
            $this->setFileName($file_name);
            $first = true;
            while (($data = fgetcsv($handle)) !== false) {
                if ($first) {
                    $this->columns = CsvFileFormater::formatColumnNames($data);
                } else {
                    $this->sanitizeData($data);
                }
                $first = false;
            }
        }
        fclose($handle);
    }

    public function sanitizeData($line)
    {
        $formated_data = UserFormater::format($line, $this->columns);
        array_push($this->sanitazedData, $formated_data);
    }

    public function getSanitazedData()
    {
        return $this->sanitazedData;
    }

    private function setFileName($file_name)
    {
        $file_name_without_extension = str_replace('.csv', '', $file_name);
        $this->file_name = $file_name_without_extension;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function getColumns()
    {
        return $this->columns;
    }
}
