<?php

namespace Cli\Helpers\Database;

use Cli\Helpers\CliPrint\CliPrinter;

class CreateDatabase extends Database
{
    private $name;
    private $database;
    protected $connection;
    public function __construct($connection, $name = null)
    {
        $this->name = $name;
        $this->connection = $connection;
        $this->database = 'CREATE DATABASE ' . $name;
    }

    public function execute()
    {
        $config = include 'config.php';
        // Make db the current database
        $name = $this->name == null ? $config['database'] : $this->name;
        $db_selected = $this->connection->select_db($name);
        if (!$db_selected) { // check if db is already created
            $sql = $this->database . $name;
            if ($this->connection->query($sql)) {
                CliPrinter::display("Database catalyst created successfully");
            } else {
                CliPrinter::display("ERROR: " . $this->connection->error);
                exit;
            }
        } else {
            CliPrinter::display("Database already created!");
        }

    }
}
