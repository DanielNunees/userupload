<?php

namespace Cli\Helpers\Database;

use Cli\Helpers\CliPrint\CliPrinter;

class CreateTable
{

    private $table;
    private $table_name;
    private $columns = [];
    private $drop;
    private $connection;
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function table($name)
    {
        $this->table_name = $name;
        $this->table = 'CREATE TABLE ' . $name . '( ';
        return $this;
    }

    public function id($size = 6)
    {
        $this->id = 'id INT(' . $size . ') UNSIGNED AUTO_INCREMENT PRIMARY KEY, ';
        return $this;
    }

    public function string($name, $lenght = 255, $not_null = false, $unique = false, $default = null)
    {
        $isNull = $not_null == true ? ' NOT NULL ' : '';
        $isUnique = $unique == true ? ' UNIQUE ' : '';
        $defaulValue = $default != null ? ' DEFAULT ' . $default : '';
        $column = $name . ' VARCHAR(' . $lenght . ')' . $isNull . $isUnique . $defaulValue;
        array_push($this->columns, $column);
        return $this;
    }

    public function dropIfExist()
    {
        $this->drop = 'DROP TABLE ' . $this->table_name . ';';
        return $this;
    }

    public function dropTable()
    {
        $dropResult = $this->connection->query($this->drop);
        return $dropResult;
    }

    public function execute()
    {
        if ($this->drop) {
            $dropResult = $this->dropTable();
        }
        if ($dropResult) {
            $comma_separated = implode(",", $this->columns);
            $sql = $this->table . $this->id . $comma_separated . ');';
            $result = $this->connection->query($sql);
            if ($result) {
                CliPrinter::display("Table created successfully");
            } else {
                CliPrinter::display("ERROR: " . $this->connection->error);
                exit;
            }
        }

    }
}
