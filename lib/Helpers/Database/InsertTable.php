<?php
// $sql = "INSERT INTO users (name, surname, email) VALUES
//             ('John', 'Rambo', 'johnrambo@mail.com'),
//             ('Clark', 'Kent', 'clarkkent@mail.com'),
//             ('John', 'Carter', 'johncarter@mail.com'),
//             ('Harry', 'Potter', 'harrypotter@mail.com')";

namespace Cli\Helpers\Database;

use Cli\Helpers\CliPrint\CliPrinter;

class InsertTable
{
    private $table_name;
    private $columns;
    private $values = [];
    private $truncate;
    private $connection;
    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function table($name)
    {
        $this->table_name = $name;
        return $this;
    }

    /**
     * define the column names
     */
    public function column(array $column_names)
    {
        $this->columns = $column_names;
        return $this;
    }

    /**
     * define the values that will be inserted
     */
    public function values($models)
    {
        $this->values = $models;
        return $this;
    }

    /**
     * create query to tuncate table
     */
    public function trucate()
    {
        $this->truncate = 'TRUNCATE TABLE ' . $this->table_name . ';';
        return $this;
    }

    /**
     * code thjat will execute the querry
     */
    public function execute()
    {
        if ($this->truncate) { ///Truncate table if required
            $this->connection->query($this->truncate);
            CliPrinter::display("Table Truncated");
        }

        // set parameters and execute
        // prepare and bind
        foreach ($this->values as $value) {
            $stmt = $this->connection->prepare("INSERT INTO " . $this->table_name . "(" . implode(", ", $this->columns) . ") VALUES (" . implode(',', array_fill(0, count($this->columns), '?')) . ")");
            if ($stmt) {
                $stmt->bind_param(str_repeat('s', count($this->columns)), ...$value->toArray());
                $stmt->execute();
            } else {
                CliPrinter::display("ERROR: " . $this->connection->error);
                exit;
            }
        }
        CliPrinter::display("Data inserted successfully");
    }
}
