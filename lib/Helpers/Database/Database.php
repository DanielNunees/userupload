<?php

namespace Cli\Helpers\Database;

use Cli\Helpers\CliPrint\CliPrinter;

class Database
{

    private $host;
    private $username;
    private $password;
    protected $connection;

    public function __construct($username, $password, $host)
    {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->connect();
    }

    public function getConnection()
    {
        return $this->connection;
    }

    private function connect()
    {
        try {
            // Create connection

            $this->connection = mysqli_connect($this->host, $this->username, $this->password);
            // Check connection
            if (!$this->connection) {
                CliPrinter::display("ERROR: Database Connection Failed");
                exit;
            }
            CliPrinter::display("Database Connected!");

        } catch (\Exception$e) {
            CliPrinter::display("ERROR: " . $this->connection->error);
            exit;
        }
    }
}
