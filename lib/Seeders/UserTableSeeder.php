<?php

namespace Cli\Seeders;

use Cli\Helpers\Database\Database;
use Cli\Helpers\Database\InsertTable;


class UserTableSeeder extends Database{
    protected $connection;
    
    public $createTable;
    public function __construct($connection)
    {
        $this->createTable = new InsertTable($connection);
    }


    public function run($models)
    {
       $this->createTable
          ->table("users")
          ->column(array('name', 'surname', 'email'))
          ->values($models)
          ->trucate()
          ->execute();

    }
}