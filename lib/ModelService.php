<?php

namespace Cli;

//Create a model based on data.
class ModelService
{

    /**
     * The column names must be the first line in the file
     */
    public static function createUserModelFromCsvFile(array $data, array $columns, string $file_name)
    {
        $models = [];
        $modelClassName = "Cli\\Models\\" . ucfirst($file_name);

        $settters = static::getClassSetters($columns, $file_name);
        foreach ($data as $value) {
            $model = new $modelClassName();
            for ($i = 0; $i < count($columns); $i++) {
                $model->{$settters[$columns[$i]]}($value[$i]);
            }
            $models[] = $model;
        }
        return $models;
    }

    public static function getClassGetters($columns, $file_name)
    {
        $gettters = [];
        $modelClassName = "Cli\\Models\\" . ucfirst($file_name);
        //get the class getters and setters
        for ($i = 0; $i < count($columns); $i++) {
            $getMethod = "get" . ucfirst($columns[$i]);
            $classMethods = get_class_methods($modelClassName);
            $gettters[$columns[$i]] = in_array($getMethod, $classMethods) ? $getMethod : null;
        }
        return $gettters;
    }

    public static function getClassSetters($columns, $file_name)
    {
        $settters = [];
        $modelClassName = "Cli\\Models\\" . ucfirst($file_name);
        //get the class getters and setters
        for ($i = 0; $i < count($columns); $i++) {
            $setMethod = "set" . ucfirst($columns[$i]);
            $classMethods = get_class_methods($modelClassName);
            $settters[$columns[$i]] = in_array($setMethod, $classMethods) ? $setMethod : null;
        }
        return $settters;
    }

}
