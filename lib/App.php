<?php

namespace Cli;

use Cli\CommandController;
use Cli\Helpers\CliPrint\CliPrinter;
use Exception;

class App
{

    protected $command_registry;

    protected $databaseService;

    protected $app_signature;

    public function __construct()
    {
        $this->command_registry = new CommandRegistry(__DIR__ . '/../app/Command');
    }

    public function registerCommand($name, $callable)
    {
        $this->command_registry->registerCommand($name, $callable);
    }

    /**
     * Here is wher the commands get executed
     * in order:
     * boot()
     * run()
     * teardown()
     * if any error is throw will be catched here
     */
    public function runCommand($argv)
    {
        try {
            $input = new CommandCall($argv);
            foreach ($input->command as $command) {
                $controller = $this->command_registry->getCallableController($command, null);
                if ($controller instanceof CommandController) {
                    $controller->boot($this);
                    $controller->run($input);
                    $controller->teardown();
                }
            }
        } catch (Exception $ex) {
            CliPrinter::display("ERROR: " . $ex->getMessage());
            exit;
        }
    }
}
