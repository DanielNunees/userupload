<?php

namespace Cli\Models;

class Users extends Model
{

    private $name;
    private $surname;
    private $email;

    public function __construct()
    {

    }

    public function toArray()
    {
        return array(
            $this->name,
            $this->surname,
            $this->email,
        );
    }

    public function getName()
    {
        return $this->name;
    }
    public function getSurname()
    {
        return $this->surname;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }
    public function setEmail($email)
    {
        $this->email = $email;
    }

}
