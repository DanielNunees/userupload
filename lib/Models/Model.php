<?php

namespace Cli\Models;

abstract class Model
{
    abstract public function toArray();
}
