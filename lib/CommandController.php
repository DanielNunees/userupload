<?php

namespace Cli;

use Cli\App;
use Cli\CommandCall;

abstract class CommandController
{
    protected $app;

    protected $input;

    protected $CsvData = [];

    protected $databaseService;

    abstract public function handle();

    public function boot(App $app)
    {
        $this->app = $app;
    }

    public function run(CommandCall $input)
    {
        $this->input = $input;
        $this->handle();
    }

    public function teardown()
    {
        //
    }

    protected function setCsvData($data)
    {
        $this->CsvData = $data;
    }

    protected function getCsvData()
    {
        return $this->CsvData;
    }

    protected function getArgs()
    {
        return $this->input->args;
    }

    protected function getParams()
    {
        return $this->input->params;
    }

    protected function getParamsKeys()
    {
        return array_keys($this->input->params);
    }

    protected function getShortOpts()
    {
        return $this->input->shortOpts;
    }

    protected function hasParam($param)
    {
        return $this->input->hasParam($param);
    }

    protected function getParam($param)
    {
        return $this->input->getParam($param);
    }

    protected function getConfigOptions()
    {
        return $this->input->getConfigOptions();
    }

    protected function getApp()
    {
        return $this->app;
    }

    protected function getDatabaseService()
    {
        return $this->databaseService;
    }

    protected function setDatabaseService($databaseService)
    {
        $this->databaseService = $databaseService;
    }

    protected function getInvalidCommands()
    {
        return $this->input->getInvalidCommands();
    }

    protected function getArgvParams()
    {
        return $this->input->getArgvParams();
    }

}
