<?php
namespace Cli;

use Cli\Helpers\Database\CreateDatabase;
use Cli\Helpers\Database\Database;
use Cli\Migrations\CreateTableUser;
use Cli\Seeders\UserTableSeeder;

// This class is a singleton
class DatabaseService
{

    private $connection;
    private static $instance = null;

    private function __construct()
    {

    }

    // The object is created from within the class itself
    // only if the class has no instance.
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new DatabaseService();
        }
        return self::$instance;
    }

    /**
     * Create the initial connection with the databsse
     */
    public function connectDatabase($username, $password, $host)
    {
        $this->connection = new Database($username, $password, $host);
    }

    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Create a new dabase if not existent config.php[database]
     */
    public function createDatabase()
    {
        $createDatabase = new CreateDatabase($this->connection->getConnection());
        $createDatabase->execute();
    }

    /**
     * Run the migration, eg. create tables
     */
    public function runMigrations()
    {
        $createUserTable = new CreateTableUser($this->connection->getConnection());
        $createUserTable->up();
    }

    /**
     * Terminate the current connection with the database
     */
    public function closeConnection()
    {
        $this->connection->getconnection()->close();
    }

    public function runSeeder($models)
    {
        $userTableSeeder = new UserTableSeeder($this->connection->getConnection());
        $userTableSeeder->run($models);
    }

}
