<?php

namespace Cli\Migrations;

use Cli\Helpers\Database\CreateTable;

class CreateTableUser
{

    public $createTable;
    public function __construct($connection)
    {
        $this->createTable = new CreateTable($connection);
    }
    /**
     * Run the migration.
     *
     * @return void
     */
    public function up()
    {

        $this->createTable
            ->table("users")
            ->id()
            ->string('name', 30, true)
            ->string('surname', 30, true)
            ->string('email', 30, false, true, 'NULL')
            ->dropIfExist()
            ->execute();
    }
}
