<?php

namespace Cli;

/**
 * This class is responsible to load the Commnad controllers classes
 * the script runs inside the folder App/Command to find any files that finishes
 * with Controller in the name file and register the class name for later use
 */
class CommandRegistry
{
    protected $commands_path;

    protected $controllers = [];

    protected $default_registry = [];

    public function __construct($commands_path)
    {
        $this->commands_path = $commands_path;
        $this->loadControllers($commands_path);
    }

    /**
     * Look for all controller inside the a folder
     */
    public function loadControllers($commands_path)
    {
        foreach (glob($commands_path . '/*Controller.php') as $controller_file) {
            $this->loadCommandMap($controller_file);
        }
        return $this->getControllers();
    }

    /**
     * register all founded controller to later use
     */
    protected function loadCommandMap($controller_file)
    {
        $filename = basename($controller_file);
        $controller_class = str_replace('.php', '', $filename);
        $command_name = strtolower(str_replace('Controller', '', $controller_class));
        $full_class_name = sprintf("App\\Command\\%s", $controller_class);

        /** @var CommandController $controller */
        $controller = new $full_class_name();
        $this->controllers[$command_name] = $controller;
    }

    /**
     * This will be responsible for figuring out which code should be called when a command is invoked
     * @throw Exception
     */
    public function getCallable($command)
    {
        $single_command = $this->getCommand($command);
        if ($single_command === null) {
            throw new \Exception(sprintf("Command \"%s\" not found.", $command));
        }

        return $single_command;
    }

    public function getCommandsPath()
    {
        return $this->commands_path;
    }

    public function registerCommand($name, $callable)
    {
        $this->default_registry[$name] = $callable;
    }

    public function getCommand($command)
    {
        return isset($this->default_registry[$command]) ? $this->default_registry[$command] : null;
    }

    public function getCallableController($command, $subcommand = null)
    {
        return $this->getController($command);
    }
    public function getControllers()
    {
        return $this->controllers;
    }

    public function getController($command_name)
    {
        return isset($this->controllers[$command_name]) ? $this->controllers[$command_name] : null;
    }
}
