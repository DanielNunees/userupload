<?php

return array(
    'database' => 'catalyst',
    'shortopts' => "u:p:h:", //Required value: username, password, host
    'shortoptsarray' => array( ///Used to validations 
        'u',
        'p',
        'h',
    ),
    'longopts' => array(
        "file:", // CSV file path command option
        "create_table", // Create table command option
        "dry_run", // Dry run command option
        "help", // Help command option
    )
);
