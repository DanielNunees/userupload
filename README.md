# README #

# Catalyst User Upload coding test

## Features

- Import a CSV file and insert the data into database
- Validate fields before inserting
- Check fo invalid commands
- ---help available for all commands

Nothing needs to be installed, just run the commands.

## Dependecies
- No dependecies

# Commands Usage
```sh
php user_upload.php 
-u mysql_uername 
-p mysql_password 
-h host 
[--create_table --dry_run --file:] 
[--help]
```
# Help Usage

```sh
php user_upload.php --help -u
```

```sh
php user_upload.php --help -u -h -p
```

# Command [--create_table]

1. Try to connect to the database
2. Reads the CSV and sanitaze all data
3. Create a new table (users) (if not exist, disable dropIfExist() on migration to remove that option ) defined on (migrations/CreateTableUser.php)
4. Seed the database with the CSV data

# Command [--dry_run]

1. Try to connect to the database
2. Reads the CSV and sanitaze all data
3. Create a new table (users) (if not exist, disable dropIfExist() on migration to remove that option ) defined on (migrations/CreateTableUser.php)
